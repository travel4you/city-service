FROM openjdk:8-jdk-alpine
MAINTAINER Piotr Pażuchowski <piotr.pazuchowski@gmail.com>

ADD ./target/city-service.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/city-service.jar"]

EXPOSE 8080