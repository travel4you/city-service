package mgr.travel4you.cityservice.service;

import mgr.travel4you.cityservice.domain.City;
import mgr.travel4you.cityservice.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CityService {
    @Autowired
    private CityRepository repository;

    /**
     * Read city list iterable.
     *
     * @return the iterable
     */
    @Cacheable(value = "cities")
    public Iterable<City> readCityList() {
        return repository.findAll();
    }

    /**
     * Read city by id optional.
     *
     * @param id the id
     * @return the optional
     */
    @Cacheable(value = "cities", key="#id")
    public Optional<City> readCityById(Long id) {
        return repository.findById(id);
    }

    /**
     * Create city city.
     *
     * @param city the city
     * @return the city
     */
    @CacheEvict(value="cities")
    public City createCity(City city) {
        return repository.save(city);
    }

    /**
     * Update city city.
     *
     * @param city the city
     * @return the city
     */
    @CachePut(value="cities", key="#city.getId()")
    public City updateCity(City city) {
        return repository.save(city);
    }

    /**
     * Delete city.
     *
     * @param city the city
     */
    @CacheEvict(value="cities", key="#city.getId()")
    public void deleteCity(City city) {
        repository.delete(city);
    }
}
