package mgr.travel4you.cityservice.repository;


import mgr.travel4you.cityservice.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The interface City repository.
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long> {

}
