package mgr.travel4you.cityservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Requested city didn't find in City repository.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Sorry, requested city doesn't exists!")
public class CityNotFoundException extends Exception {
    private static final long serialVersionUID = 100L;
}