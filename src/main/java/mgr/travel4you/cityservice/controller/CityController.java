package mgr.travel4you.cityservice.controller;

import mgr.travel4you.cityservice.domain.City;
import mgr.travel4you.cityservice.dto.CityReadDto;
import mgr.travel4you.cityservice.dto.CityUpdateDto;
import mgr.travel4you.cityservice.exception.CityNotFoundException;
import mgr.travel4you.cityservice.service.CityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class CityController {

    @Autowired
    private CityService cityService;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Read city list list.
     *
     * @return the list
     */
    @GetMapping(path = "/city")
    public List<CityReadDto> readCityList() {
        return StreamSupport
                .stream(cityService.readCityList().spliterator(), true)
                .map(city -> modelMapper.map(city, CityReadDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Read city response entity.
     *
     * @param id the id
     * @return the response entity
     * @throws CityNotFoundException the city not found exception
     */
    @GetMapping(path = "/city/{id}")
    public ResponseEntity<CityReadDto> readCity(@PathVariable Long id) throws CityNotFoundException {
        return cityService
                .readCityById(id)
                .map(city -> modelMapper.map(city, CityReadDto.class))
                .map(city -> ResponseEntity.ok().body(city))
                .orElseThrow(CityNotFoundException::new);
    }

    /**
     * Update city response entity.
     *
     * @param id   the id
     * @param body the body
     * @return the response entity
     * @throws CityNotFoundException the city not found exception
     */
    @PutMapping(path = "/city/{id}")
    public ResponseEntity<Void> updateCity(@PathVariable Long id, @RequestBody @Valid CityUpdateDto body) throws CityNotFoundException {
        City fetchedCity = cityService
                .readCityById(id)
                .orElseThrow(CityNotFoundException::new);

        City cityMappedByDto = modelMapper.map(body, City.class);

        cityMappedByDto.setId(id);
        cityMappedByDto.setCreatedAt(fetchedCity.getCreatedAt());

        cityService.updateCity(cityMappedByDto);

        return ResponseEntity.noContent().build();

    }

    /**
     * Delete city response entity.
     *
     * @param id the id
     * @return the response entity
     * @throws CityNotFoundException the city not found exception
     */
    @DeleteMapping(path = "/city/{id}")
    public ResponseEntity<Void> deleteCity(@PathVariable Long id) throws CityNotFoundException  {
        City city = cityService
                .readCityById(id)
                .orElseThrow(CityNotFoundException::new);

        cityService.deleteCity(city);

        return ResponseEntity.noContent().build();
    }
}
