package mgr.travel4you.cityservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class City implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;

    @NotNull
    private LocalDateTime createdAt;

    @NotNull
    private LocalDateTime editedAt;

    public Integer countryId;

    public String name;

    @Column(length = 2048)
    public String description;
    public String population;
    public String territory;

    public Double latitude;
    public Double longitude;

    public String imageUrl;
}
