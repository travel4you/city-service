package mgr.travel4you.cityservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * The City Dto dedicated for update action.
 */
public class CityUpdateDto {
    @Id
    @NotNull
    private Long id;

    @JsonIgnore
    private LocalDateTime editedAt = LocalDateTime.now();

    public Integer countryId;

    public String name;
    public String description;
    public String population;
    public String territory;

    public Double latitude;
    public Double longitude;

    public String imageUrl;
}
