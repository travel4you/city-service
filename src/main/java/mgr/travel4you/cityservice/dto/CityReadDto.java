package mgr.travel4you.cityservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;

/**
 * The City Dto dedicated for read action.
 */
@Getter
@Setter
@NoArgsConstructor
public class CityReadDto {
    @Id
    private Long id;

    public Integer countryId;

    public String name;
    public String description;
    public String population;
    public String territory;

    public Double latitude;
    public Double longitude;

    public String imageUrl;
}
