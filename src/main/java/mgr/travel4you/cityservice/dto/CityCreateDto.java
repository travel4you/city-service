package mgr.travel4you.cityservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * The City Dto dedicated for create action.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CityCreateDto {
    @Id
    public Long id;

    @JsonIgnore
    private LocalDateTime createdAt = LocalDateTime.now();

    @JsonIgnore
    private LocalDateTime editedAt = LocalDateTime.now();

    public Integer countryId;

    public String name;
    public String description;
    public String population;
    public String territory;

    public Double latitude;
    public Double longitude;

    public String imageUrl;
}
