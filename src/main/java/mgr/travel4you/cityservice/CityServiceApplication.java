package mgr.travel4you.cityservice;

import mgr.travel4you.cityservice.domain.City;
import mgr.travel4you.cityservice.dto.CityCreateDto;
import mgr.travel4you.cityservice.service.CityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class CityServiceApplication {

	@Autowired
	private CityService cityService;

	@Autowired
	private ModelMapper modelMapper;

	public static void main(String[] args) {
		SpringApplication.run(CityServiceApplication.class, args);
	}

	@PostConstruct
	public void run() {

		CityCreateDto c1 = new CityCreateDto();
		c1.setName("Warszawa");
		c1.setDescription("Warszawa, miasto stoleczne Warszawa – stolica Polski i wojewodztwa mazowieckiego, najwieksze miasto kraju, polozone w jego srodkowo-wschodniej czesci, na Nizinie Srodkowomazowieckiej, na Mazowszu, nad Wisla.");
		c1.setCountryId(1);
		c1.setPopulation("1,735 miliona (2015)");
		c1.setTerritory("517 km");
		c1.setLatitude(52.229676);
		c1.setLongitude(21.012229);
		c1.setImageUrl("/static/rsz_duzy-budynek-biurowy_1160-373.jpg");

		CityCreateDto c2 = new CityCreateDto();
		c2.setName("Wrocław");
		c2.setDescription("Wrocław - miasto na prawach powiatu w południowo-zachodniej Polsce, siedziba władz województwa dolnośląskiego i powiatu wrocławskiego. Położone w Europie Środkowej, na Nizinie Śląskiej, w Pradolinie Wrocławskiej, nad rzeką Odrą i czterema jej dopływami. Jest historyczną stolicą Dolnego Śląska, a także całego Śląska");
		c2.setCountryId(1);
		c2.setPopulation("628 589 (2015)");
		c2.setTerritory("292,9 km");
		c2.setLatitude(51.107885);
		c2.setLongitude(17.038538);
		c2.setImageUrl("/static/rsz_university-in-wroclaw-1-1231133.jpg");

		CityCreateDto c3 = new CityCreateDto();
		c3.setName("Zakopane");
		c3.setDescription("Zakopane – miasto w wojewodztwie malopolskim, siedziba powiatu tatrzanskiego. Zakopane jest najwiekszym osrodkiem miejskim w bezposrednim otoczeniu Tatr, duzym osrodkiem sportow zimowych, od dawna nazywane nieformalnie zimowa stolica Polski.");
		c3.setCountryId(1);
		c3.setPopulation("27 424 (2006)");
		c3.setTerritory("84 km");
		c3.setLatitude(49.299181);
		c3.setLongitude(19.949562);
		c3.setImageUrl("/static/rsz_zakopane-fairyland-look-1315058.jpg");

		City city1 = modelMapper.map(c1, City.class);
		City city2 = modelMapper.map(c2, City.class);
		City city3 = modelMapper.map(c3, City.class);

		this.cityService.createCity(city1);
		this.cityService.createCity(city2);
		this.cityService.createCity(city3);
	}
}
